## Usage

```python
import foobar

# returns 'server+local'
foobar.pluralize('server+local')

# returns 'geese'
foobar.pluralize('goose')

# returns 'local'
foobar.singularize('local') ****
# returns 'server'
foobar.singularize('server') ****
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.
